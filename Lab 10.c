#include <stdio.h>
int main()
{
    struct student{
        int roll_no;
        char sname[50];
        char section[10];
        char depart[100];
        int fees;
        int result;
    }s1,s2;
    printf("Enter the student 1 details - roll_no,name,section,department,fees and result");
    scanf("%d%s%s%s%d%d",&s1.roll_no,s1.sname,s1.section,s1.depart,&s1.fees,&s1.result);
    printf("Enter the student 2 details - roll_no,name,section,department,fees and result");
    scanf("%d%s%s%s%d%d",&s2.roll_no,s2.sname,s2.section,s2.depart,&s2.fees,&s2.result);
    if(s1.result>s2.result)
        printf("The student 1 scored the highest marks\nroll number : %d\n name:%s\nsection:%s\ndepartment:%s\nfees:%d\nresult:%d",s1.roll_no,s1.name,s1.section,s1.depart,s1.fees,s1.result);
    else
        printf("The student 2 scored the highest marks\nroll number : %d\n name:%s\nsection:%s\ndepartment:%s\nfees:%d\nresult:%d",s2.roll_no,s2.name,s2.section,s2.depart,s2.fees,s2.result);
    return 0;
}