#include <stdio.h>
#include <math.h>
int main()
{
    int a,b,c;
    float D,r1,r2,real,imaginary;
    printf("Enter the value of a,b,c\n");
    scanf("%d %d %d",&a,&b,&c);
    D=(b*b)-(4*a*c);
    if(D>1)
    {
        printf("Roots are real\n");
        r1=-b+(sqrt(D))/(2*a);
        r2=+b-(sqrt(D))/(2*a);
        printf("%f %f",r1,r2);
    }
    else if(D==0)
    {
        printf("Roots are equal\n");
        r1=(-b)/(2*a);
        printf("%f",r1);
    }
    else if(D<0)
    {
       printf("Roots are imaginary\n");
       real=(-b/(2*a));
       imaginary=sqrt(-D)/(2*a);
       printf("Roots  %f + i%f",real,imaginary);
    }
}
